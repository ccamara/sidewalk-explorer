# Web Map GL App Starter

A starter Stencil application using Ionic and
[Web Map GL](https://gitlab.com/ccrpc/webmapgl).

## License
Web Map GL App Starter is available under the terms of the [BSD 3-clause
license](https://gitlab.com/ccrpc/sidewalk-explorer/blob/master/LICENSE.md).
