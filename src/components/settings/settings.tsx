import { Component, Element, Prop, State } from '@stencil/core';
import { getState, State as AppState } from '../utils';


@Component({
  tag: 'se-settings'
})
export class Settings {
  mapEl?: HTMLGlMapElement;
  root?: HTMLSeRootElement;

  @Element() el: HTMLElement;

  @State() state: AppState;

  @Prop({connect: 'ion-modal-controller'}) modalCtrl!:
    HTMLIonModalControllerElement;

  componentWillLoad() {
    this.root = document.querySelector('se-root');
    this.mapEl = this.root.querySelector('gl-map');
    this.updateState();
  }

  componentDidUpdate() {
    // FIXME: This is a workaround for an Ionic/Stencil bug that prevents
    // options from being removed from an ion-select:
    // https://github.com/ionic-team/ionic/issues/17344
    // https://github.com/ionic-team/stencil/issues/1218
    let event = document.createEvent('Event');
    event.initEvent('ionSelectOptionDidUnload', true, true);
    let selects = this.el.querySelectorAll('ion-select');
    Array.from(selects).forEach((select) => select.dispatchEvent(event));
  }

  updateState() {
    this.state = getState();
  }

  setFeatureType(value: string) {
    this.root.featureType = value;
    this.updateState();
  }

  setField(value: string) {
    this.root.field = value;
    this.updateState();
  }

  setRegion(value: string) {
    this.root.region = value;

    this.updateState();
    let region = this.state.region;
    if (region.bbox && region.bbox.length === 4)
      this.mapEl.map.fitBounds(region.bbox);
  }

  setYear(value: string) {
    this.root.year = value;
    this.updateState();
  }

  getOptions(selector: string) : HTMLIonSelectOptionElement[] {
    return Array.from(this.root.querySelectorAll(selector))
      .map((region) => (
        <ion-select-option value={(region as any).name}>
          {(region as any).label}
        </ion-select-option>
      )) as HTMLIonSelectOptionElement[];
  }

  getSelectItem(value: string, handler: Function,
      options: HTMLIonSelectOptionElement[], label: string) {
    return (
      <ion-item>
        <ion-label>{label}</ion-label>
        <ion-select value={value}
            onIonChange={(e) => handler.bind(this)(e.detail.value)}>
          {options}
        </ion-select>
      </ion-item>
    );
  }

  async closeModal() {
    const modalCtrl = await this.modalCtrl.componentOnReady();
    modalCtrl.dismiss();
  }

  render() {
    let featureTypeOptions = this.getOptions('se-feature-type');
    let fieldOptions = this.getOptions(
      `se-feature-type[name='${this.state.featureType.name}'] se-field`);
    let regionOptions = this.getOptions('se-region');
    let yearOptions = this.getOptions(
      `se-region[name='${this.state.region.name}'] se-year`);

    return ([
      <ion-header>
        <ion-toolbar>
          <ion-title>View Options</ion-title>
          <ion-buttons slot="end">
            <ion-button onClick={() => this.closeModal()}>
              <ion-icon slot="start" name="checkmark"></ion-icon>
              Done
            </ion-button>
          </ion-buttons>
        </ion-toolbar>
      </ion-header>,
      <ion-content>
        <ion-list>
          {this.getSelectItem(
            this.state.featureType.name, this.setFeatureType,
            featureTypeOptions, 'Feature Type')}
          {this.getSelectItem(
            this.state.field.name, this.setField, fieldOptions, 'Score Type')}
          {this.getSelectItem(
            this.state.region.name, this.setRegion, regionOptions, 'Region')}
          {this.getSelectItem(
            this.state.year.name, this.setYear, yearOptions, 'Year')}
        </ion-list>
      </ion-content>
    ]);
  }
}
