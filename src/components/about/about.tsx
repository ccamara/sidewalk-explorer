import { Component, Prop } from '@stencil/core';


@Component({
  tag: 'se-about'
})
export class About {
  @Prop({connect: 'ion-modal-controller'}) modalCtrl!:
    HTMLIonModalControllerElement;

  async closeModal() {
    const modalCtrl = await this.modalCtrl.componentOnReady();
    modalCtrl.dismiss();
  }

  render() {
    return ([
      <ion-header>
        <ion-toolbar>
          <ion-title>About Sidewalk Explorer</ion-title>
        </ion-toolbar>
      </ion-header>,
      <ion-content>
        <ion-img src="assets/images/about-banner.jpg"
          alt="Banner graphic showing a curb ramp, a data collection tablet, a pedestrian signal pushbutton, and two data collection staff using a measuring wheel and smart slope meter."></ion-img>
        <ion-grid>
          <ion-row>
            <ion-col>
              <p>
                Welcome to Sidewalk Explorer, an interactive viewer for
                sidewalk network features created by
                the <a href="https://ccrpc.org/">Champaign County Regional
                Planning Commission</a>. You can explore current
                and historical trends in condition and Americans with
                Disabilities Act (ADA) compliance for sidewalks, curb ramps,
                crosswalks, and pedestrian signals in the public right-of-way.
              </p>
              <p>
                The ADA compliance scores reflect preliminary compliance
                with the proposed <a href="https://www.access-board.gov/guidelines-and-standards/streets-sidewalks/public-rights-of-way/proposed-rights-of-way-guidelines">
                Public Right-of-Way Accessibility Guidelines
                (PROWAG)</a>. Condition scores reflect other factors that
                that impact the usability sidewalks and curb ramps.
              </p>
              <p>
                The assessment scores are one of several factors local agencies
                use in prioritizing sidewalk network improvements. For a
                discussion of other factors influencing prioritization
                and for details about the data collection process, download
                the Sidewalk Network Inventory and Assessment report.
              </p>
            </ion-col>
          </ion-row>
          <ion-row>
            <ion-col>
              <ion-button expand="block" onClick={() => this.closeModal()}>
                <ion-icon slot="start" name="pin"></ion-icon>
                Start Exploring
              </ion-button>
            </ion-col>
            <ion-col>
              <a href="http://www.ccrpc.org/wp-content/uploads/2016/02/SidewalkNetworkInventoryAssessment.pdf">
                <ion-button expand="block" color="light">
                  <ion-icon slot="start" name="download"></ion-icon>
                  Download Report (22 MB)
                </ion-button>
              </a>
            </ion-col>
          </ion-row>
        </ion-grid>
      </ion-content>
    ]);
  }
}
