import { Component, Element, Listen, Prop, Watch } from '@stencil/core';
import bbox from '@turf/bbox';
import { getStateByNames, doOnce } from '../utils';


@Component({
  tag: 'se-root',
  styleUrl: 'root.css'
})
export class Root {
  ignoreClick: boolean = false;
  mapEl?: HTMLGlMapElement;
  styleEl?: HTMLGlStyleElement;

  @Element() el: HTMLSeRootElement;

  @Prop() baseUrl: string;
  @Prop() featureType: string;
  @Prop() field: string;
  @Prop() region: string;
  @Prop({mutable: true}) selectedFeature: any;
  @Prop() year: string;

  componentDidLoad() {
    this.updateFields();
    this.updateCounts();
    this.mapEl.map.on('click', () => {
      if (!this.ignoreClick) this.deselectFeature();
    });
    doOnce('se.intro', () => this.showIntro());
  }

  @Listen('glFeatureClick')
  handleClick(e: CustomEvent) {
    let feature = e.detail.features[0];
    if (!feature) return;
    if (feature.sourceLayer == 'aggregated') {
      this.mapEl.map.fitBounds(bbox(feature));
    } else {
      this.selectedFeature = feature;
    }
    this.ignoreClick = true;
    window.setTimeout(() => this.ignoreClick = false, 100);
  }

  @Watch('featureType')
  @Watch('region')
  @Watch('year')
  deselectFeature() {
    this.selectedFeature = null;
  }

  @Watch('featureType')
  @Watch('field')
  @Watch('selectedFeature')
  updateFields() {
    Array.from(this.el.querySelectorAll('se-feature-type')).forEach((ft) => {
      Array.from(ft.querySelectorAll('se-field')).forEach((field) => {
        field.visible = (!this.selectedFeature &&
          ft.name === this.featureType && field.name === this.field);
      });
    });
  }

  @Watch('region')
  @Watch('year')
  async updateCounts() {
    let state = getStateByNames(
      this.featureType, this.field, this.region, this.year);
    let region = state.region;
    let year = state.year;

    this.assignCounts('current', region.name, year.name);
    if (region.baselineYear != year.name) {
      this.assignCounts('baseline', region.name, region.baselineYear);
    } else {
      this.unassignCounts('baseline');
    }
  }

  async assignCounts(prop: 'current' | 'baseline',
      region: string, year: string) {
    let res = await fetch(`assets/summaries/${region}-${year}.json`);
    let data = await res.json();

    Array.from(this.el.querySelectorAll('se-feature-type'))
      .forEach((ft) => {
        Array.from(ft.querySelectorAll('se-field'))
          .forEach((field) => field[prop] = data[ft.value][field.value]);
      });
  }

  async unassignCounts(prop: 'current' | 'baseline') {
    Array.from(this.el.querySelectorAll('se-field'))
      .forEach((field) => field[prop] = null);
  }

  getFeatureDetails() {
    if (!this.selectedFeature) return;
    return (
      <se-feature-detail feature={this.selectedFeature}></se-feature-detail>
    );
  }

  showIntro() {
    this.el.querySelector('se-about-button').openModal(null);
  }

  render() {
    let state = getStateByNames(
      this.featureType, this.field, this.region, this.year);
    let imageUrl : string = null;
    if (this.selectedFeature) {
      let image = this.selectedFeature.properties.image;
      if (image) imageUrl = state.featureType.imageUrl + image;
    } else {
      imageUrl = `assets/images/` +
        `${state.featureType.name}-${state.field.name}.jpg`;
    }
    let secondary = (this.selectedFeature) ?
      'Feature Details' : state.field.label;

    return (
      <gl-app label="Sidewalk Explorer" menu={false} splitPane={false}>
        <se-settings-button slot="end-buttons"></se-settings-button>
        <gl-basemaps slot="end-buttons"></gl-basemaps>
        <gl-fullscreen slot="end-buttons"></gl-fullscreen>
        <se-about-button slot="end-buttons"></se-about-button>
        <div class="panes">
          <gl-map class="map-pane"
              longitude={-88.228878} latitude={40.110319} zoom={12} maxzoom={22}
              ref={(r: HTMLGlMapElement) => this.mapEl = r}>
            <se-style state={state}></se-style>
            <gl-style url="https://maps.ccrpc.org/basemaps/basic/style.json"
              basemap={true}
              thumbnail="https://maps.ccrpc.org/basemaps/basic/preview.jpg"
              name="Basic" enabled={true}></gl-style>
            <gl-style url="https://maps.ccrpc.org/basemaps/hybrid/style.json"
              basemap={true}
              thumbnail="https://maps.ccrpc.org/basemaps/hybrid/preview.jpg"
              name="2017 Aerial Imagery" enabled={false}></gl-style>
          </gl-map>
          <div class="info-pane">
            <se-graphic-header hasSelection={this.selectedFeature}
                primary={state.featureType.label}
                secondary={secondary}
                image={imageUrl}></se-graphic-header>
            <div class="gl-menu-content">
              <slot name="menu" />
              {this.getFeatureDetails()}
            </div>
          </div>
        </div>
      </gl-app>
    );
  }
}
