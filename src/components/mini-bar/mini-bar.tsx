import { Component, Prop } from '@stencil/core';
import { getScoreColor } from '../utils';


@Component({
  styleUrl: 'mini-bar.css',
  tag: 'se-mini-bar'
})
export class MiniBar {
  @Prop() score: number;

  render() {
    return (
      <div class="bar" style={{
        backgroundColor: getScoreColor(this.score),
        width: `${this.score}%`
      }}></div>
    )
  }
}
