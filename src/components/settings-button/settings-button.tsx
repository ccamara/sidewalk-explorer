import { Component, Prop } from '@stencil/core';


@Component({
  tag: 'se-settings-button'
})
export class SettingsButton {

  @Prop({connect: 'ion-modal-controller'}) modalCtrl!:
    HTMLIonModalControllerElement;

  async showSettings(e: UIEvent) {
    const options = {
      component: document.createElement('se-settings'),
      ev: e
    };
    const modal = await this.modalCtrl.create(options);
    await modal.present();
    return modal;
  }

  render() {
    return (
      <ion-button onClick={(e) => this.showSettings(e)} title="View options">
        <ion-icon slot="icon-only" name="settings"></ion-icon>
      </ion-button>
    )
  }
}
