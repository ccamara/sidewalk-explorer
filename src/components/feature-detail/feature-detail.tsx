import { Component, Prop } from '@stencil/core';
import { getState, State as AppState } from '../utils';


@Component({
  styleUrl: 'feature-detail.css',
  tag: 'se-feature-detail'
})
export class FeatureDetail {
  @Prop() feature: any;

  getPropValue(prop: string, defaultValue: any) {
    let value = this.feature.properties[prop];
    return (value === null || value === undefined) ?
      defaultValue : Math.round(value);
  }

  setField(e: Event, name: string) {
    e.preventDefault();
    let root = document.querySelector('se-root');
    root.field = name;
    root.selectedFeature = null;
  }

  getListItems(state: AppState) {
    return Array.from(state.featureType.querySelectorAll('se-field-group'))
      .map((group) => {
        let groupItems = [
          <ion-item-divider>
            <ion-label>{group.label}</ion-label>
          </ion-item-divider>
        ];
        let fieldItems = Array.from(group.querySelectorAll('se-field'))
          .map((field) => {
            let label = (field.summary) ?
              <strong>{field.label}</strong> : field.label;
            return (
              <ion-item>
                <ion-label>
                  <a href="#" class="field-link"
                      onClick={(e) => this.setField(e, field.name)}>
                    {label}
                  </a>
                </ion-label>
                <span class="score" slot="end">
                  {this.getPropValue(field.value, '—')}
                </span>
                <se-mini-bar slot="end"
                  score={this.getPropValue(field.value, 0)}>
                </se-mini-bar>
              </ion-item>
            );
          });
        groupItems.push.apply(groupItems, fieldItems);
        return groupItems;
      });
  }

  render() {
    let state = getState();
    return (
      <ion-list>
        {this.getListItems(state)}
      </ion-list>
    );
  }
}
