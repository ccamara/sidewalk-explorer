import { Component, Element, Prop } from '@stencil/core';


@Component({
  styleUrl: 'lightbox.css',
  tag: 'se-lightbox'
})
export class Lightbox {
  @Element() el: HTMLElement;

  @Prop({connect: 'ion-modal-controller'}) modalCtrl!:
    HTMLIonModalControllerElement;

  @Prop() image: string;

  async closeModal() {
    const modalCtrl = await this.modalCtrl.componentOnReady();
    modalCtrl.dismiss();
  }

  hostData() {
    return {
      style: {
        backgroundImage: (this.image) ? `url('${this.image}')` : 'none'
      }
    };
  }

  render() {
    return ([
      <ion-header translucent={true}>
        <ion-toolbar>
          <ion-title>Feature Image</ion-title>
          <ion-buttons slot="end">
            <ion-button onClick={() => this.closeModal()}>
              <ion-icon slot="start" name="checkmark"></ion-icon>
              Close
            </ion-button>
          </ion-buttons>
        </ion-toolbar>
      </ion-header>
    ]);
  }
}
