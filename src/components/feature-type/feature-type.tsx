import { Component, Prop } from '@stencil/core';


@Component({
  tag: 'se-feature-type'
})
export class FeatureType {
  @Prop() countLabel: string;
  @Prop() defaultField: string = 'compliance';
  @Prop() imageUrl: string;
  @Prop() label: string;
  @Prop() name: string;
  @Prop() percentLabel: string;
  @Prop() value: string;

  render() {
    return <slot />;
  }
}
