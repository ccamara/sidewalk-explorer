import { Component, Element } from '@stencil/core';


@Component({
  tag: 'se-deselect-button'
})
export class DeselectButton {

  @Element() el: HTMLElement;

  handleClick() {
    this.el.closest('se-root').selectedFeature = null;
  }

  render() {
    return (
      <ion-button fill="clear" color="light"
          onClick={() => this.handleClick()} title="Deselect feature">
        <ion-icon slot="icon-only" name="close"></ion-icon>
      </ion-button>
    );
  }

}
