export interface State {
  region?: HTMLSeRegionElement;
  year?: HTMLSeYearElement;
  featureType?: HTMLSeFeatureTypeElement;
  field?: HTMLSeFieldElement;
}

export const colors = [
  '#2e80bf', '#80cee8', '#fcfc5d', '#ffb061', '#e61c1f'];

export function getScoreColor(score: number) {
  if (score > 90) return colors[0];
  if (score > 80) return colors[1];
  if (score > 70) return colors[2];
  if (score > 60) return colors[3];
  return colors[4];
}

export function getStateByNames(
    featureType: string, field: string, region: string, year: string) : State {
  let regionEl: HTMLSeRegionElement =
    document.querySelector(`se-region[name='${region}']`) ||
    document.querySelector('se-region');

  let yearEl: HTMLSeYearElement =
    regionEl.querySelector(`se-year[name='${year}']`) ||
    regionEl.querySelector('se-year');

  let featureTypeEl: HTMLSeFeatureTypeElement =
    document.querySelector(`se-feature-type[name='${featureType}']`) ||
    document.querySelector('se-feature-type');

  let fieldEl: HTMLSeFieldElement =
    featureTypeEl.querySelector(`se-field[name='${field}']`) ||
    featureTypeEl.querySelector('se-field');

  return {
    region: regionEl,
    year: yearEl,
    featureType: featureTypeEl,
    field: fieldEl
  };
}

export function getState(): State {
  let root = document.querySelector('se-root');
  return getStateByNames(root.featureType, root.field, root.region, root.year);
}

export function doOnce(key: string, action: Function) {
  let done = localStorage.getItem(key) === 'true';
  if (!done) {
    localStorage.setItem(key, 'true');
    action();
  }
}
