import { Component, Prop } from '@stencil/core';


@Component({
  tag: 'se-year'
})
export class Year {
  @Prop() label: string;
  @Prop() name: string;
  @Prop() value: string;

  render() {
    return null;
  }
}
